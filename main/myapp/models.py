from django.db import models

# Create your models here.
class Person(models.Model):
    name = models.CharField(max_length=50)
    age = models.IntegerField()
    date = models.DateField(auto_now_add=True)
    gender = models.CharField(max_length=50)
    weight = models.IntegerField()
    height = models.IntegerField()
    status = models.CharField(max_length=1, default='N')

# การอ่านข้อมูลจาก models
    def __str__(self) :
        return "ชื่อ : " + self.name + "," + " อายุ : " + str(self.age) + "," + " เพศ : " + self.gender 