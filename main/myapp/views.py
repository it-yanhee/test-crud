#หน้าแสดงผลรับส่งข้อมูลต่างๆไปหา User
from django.shortcuts import render,redirect
from django.http import HttpResponse 
#Response กลับไปหา User
from myapp.models import Person
# นำ Model มาแสดงในหน้า Views
from django.contrib import messages
from django.http import JsonResponse
# Import ตัว Message
# Create your views here.

def clear_toast_session(request):
    request.session.pop('show_toast', None)
    request.session.pop('toast_message', None)
    return JsonResponse({'status': 'success'})

def index(request):
    # all_person = Person.objects.all()
    all_person = Person.objects.filter(status='N')
    # request.session['show_toast'] = True
    # request.session['toast_message'] = "ยินดีต้อนรับสู่ Website"
    return render(request,"index.html",{"all_person":all_person})
    

def about(request):
    return render(request,"about.html")

def form(request):
    if request.method == "POST":
        # รับข้อมูล
        name = request.POST["name"]
        age = request.POST["age"]
        print(name,age)
        # บันทึกข้อมูล
        person = Person.objects.create(
            name=name,
            age=age,
            status="N"
        )
        person.save()
        # messages.success(request,"บันทึกข้อมูลเรียบร้อย")
        request.session['show_toast'] = True
        request.session['toast_message'] = "บันทึกข้อมูลเรียบร้อย"
        #เปลี่ยนเส้นทาง
        return redirect("/")
    else :
        return render(request,"form.html")
    
def edit(request,person_id):
    if request.method == "POST":
        person = Person.objects.get(id=person_id)
        person.name = request.POST["name"]
        person.age = request.POST["age"]
        person.status = "N"
        person.save()
        # messages.success(request,"อัพเดตข้อมูลเรียบร้อย")
        request.session['show_toast'] = True
        request.session['toast_message'] = "อัพเดตข้อมูลเรียบร้อย"
        #เปลี่ยนเส้นทาง
        return redirect("/")
    else:
    # ดึงข้อมูลพนักงาน
        person = Person.objects.get(id=person_id)
        return render(request,"edit.html",{"person":person})
    
def delete(request,person_id):
    person = Person.objects.get(id=person_id)
    person.status = "Y"
    person.save()
    # person.delete()
    # messages.success(request,"ลบข้อมูลเรียบร้อย")
    request.session['show_toast'] = True
    request.session['toast_message'] = "ลบข้อมูลเรียบร้อย"
    #เปลี่ยนเส้นทาง
    return redirect("/")