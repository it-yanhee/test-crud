from django.urls import path 
# คำสั่งที่ ดึง ตัวแปร path จาก urls ใน main มาใช้
from myapp import views
from .views import clear_toast_session
# คำสั่งที่ ดึง views จาก myapp มาใช้
urlpatterns = [
    path('',views.index),
    path('about',views.about),
    path('form',views.form),
    path('edit/<person_id>',views.edit),
    path('delete/<person_id>',views.delete),
    path('clear_toast_session/', clear_toast_session, name='clear_toast_session')
]